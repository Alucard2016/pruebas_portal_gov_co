import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Tramites } from '../models/tramites';


@Injectable({
  providedIn: 'root'
})
export class TramitesService {

  tramites$: AngularFireList<Tramites>;

  constructor(private af: AngularFireDatabase) {
    this.getTramites();
  }

  getTramites():AngularFireList<Tramites>{
    this.tramites$ = this.af.list('/tramites');
    return this.tramites$;
  }
}
