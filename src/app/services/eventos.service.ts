import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Eventos } from '../models/eventos';

@Injectable({
  providedIn: 'root'
})
export class EventosService {

  eventos$: AngularFireList<Eventos>;

  constructor(private af: AngularFireDatabase) {
    this.getEventos();
  }

  getEventos():AngularFireList<Eventos>{
    this.eventos$ = this.af.list('/eventos');
    return this.eventos$;
  }
}
