import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  comments$: AngularFireList<any>;


  constructor(private af: AngularFireDatabase) {
    this.comments$ = this.af.list('/comments');
  }

  addComment(value: string): void {
    this.comments$.push({ comment: value });
  }

}
