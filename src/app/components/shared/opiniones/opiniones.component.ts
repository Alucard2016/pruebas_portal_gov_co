import { Component, OnInit , ViewChild, TemplateRef } from '@angular/core';
import { FormControl, FormBuilder , FormGroup, Validators } from '@angular/forms';
import { CommentsService } from 'src/app/services/comments.service';
import * as jQuery from 'jquery';
import { EventosService } from 'src/app/services/eventos.service';
import { Eventos } from 'src/app/models/eventos';

@Component({
  selector: 'app-opiniones',
  templateUrl: './opiniones.component.html',
  styleUrls: ['./opiniones.component.scss']
})
export class OpinionesComponent implements OnInit {

  eventos:Eventos[] = new Array();

  public _registerForm:FormGroup;
  public submitted:boolean = false;

  get f() { return this._registerForm.controls; }

  constructor(
    private formBuilder: FormBuilder,
    private commentsService:CommentsService,
    private eventosService:EventosService
  ) { }

  ngOnInit() {
    this._registerForm = this.formBuilder.group ({
      comentario: new FormControl('' , [Validators.required , Validators.minLength(10) , Validators.maxLength(255) ])
    });

    this.eventosService.getEventos().snapshotChanges().subscribe( data => {
      this.eventos = [];

      data.forEach( item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;

        this.eventos.push(a as Eventos);
      } )
    })

  }

  onSubmitForm(){

    this.submitted = true;

    if (this._registerForm.invalid) {
      return
    }

    this.commentsService.addComment(this.f.comentario.value);

    this._registerForm.reset();
    this.submitted = false;
    

    jQuery('#btnAbrirModal').click();

  }

}
