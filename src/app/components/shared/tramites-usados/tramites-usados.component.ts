import { Component, OnInit } from '@angular/core';
import { Tramites } from 'src/app/models/tramites';
import { TramitesService } from 'src/app/services/tramites.service';

@Component({
  selector: 'app-tramites-usados',
  templateUrl: './tramites-usados.component.html',
  styleUrls: ['./tramites-usados.component.scss']
})
export class TramitesUsadosComponent implements OnInit {

  tramites:Tramites[] = new Array();

  constructor(private tramitesService:TramitesService) { 
    
  }

  ngOnInit() { 
    this.tramitesService.getTramites().snapshotChanges().subscribe( data => {

      this.tramites = [];

      data.forEach( item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;

        this.tramites.push(a as Tramites);
      } )
    })
  }

}
